from sqlalchemy import create_engine
from airport_data import Airport
from sqlalchemy.orm import sessionmaker
import csv
import requests
import sqlalchemy

print('start of the import')

DATA_URL = "https://datahub.io/core/airport-codes/r/airport-codes.csv"

engine = create_engine('mysql://root:pass@mysqlc:3306/', echo=False)
engine.execute("""create database if not exists airports_data""")
engine = create_engine('mysql://root:pass@mysqlc:3306/airports_data?use_unicode=1&charset=utf8', echo=False)
if not sqlalchemy.inspect(engine).has_table('airports_data'):
    Airport.metadata.create_all(engine)
else:
    Airport.metadata.drop_all(engine)
    Airport.metadata.create_all(engine)

engine.execute("""ALTER TABLE airports_data CONVERT TO CHARACTER SET utf8""")

Session = sessionmaker(bind=engine)
session = Session()


print('import point 2')

with requests.Session() as s:
    decoded_content = s.get(DATA_URL).content.decode('utf-8')
    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    data_list = list(cr)
    i = 0
    for row in data_list:
        if not i:
            i += 1
            continue
        try:
            coords = row[-1].split(', ')
            session.merge(Airport(row[0], row[1], row[2], float(coords[1]), float(coords[0])))
        except:
            print('BROKEN::', row)

session.commit()

