import telebot
from distance_and_search import search, distance
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

BOT_TOKEN = '1781266905:AAEVGGOo0ZdD3zlectE6EJENkYZ0xTcra8k'
KEY1 = 'UUDD'  # Домодедово
KEY2 = 'ULLI'  # Пулково
KEY3 = 'UUEE'  # Шереметьево

bot = telebot.TeleBot(BOT_TOKEN)

engine = create_engine('mysql://root:pass@mysqlc:3306/airports_data?use_unicode=1&charset=utf8', echo=False)
Session = sessionmaker(bind=engine)
session = Session()
out_data = []


print('database connected')


@bot.message_handler(commands=['start'])
def start_command(message):
    bot.send_message(message.from_user.id, "To receive main airport information, type airport ID \n\n"
                                           "To receive distance between airports type their ID separated by space")


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.text == 'help':
        bot.send_message(message.from_user.id, "To receive main airport information, type airport ID \n\n"
                                               "To receive distance between airports type their ID separated by space")
        return
    else:
        request = message.text.split()
        if len(request) < 1 or len(request) > 2:
            bot.send_message(message.from_user.id, "Wrong number of parameters")
            return
        elif len(request) == 1:
            out_data.append(search(session, request[0]))
            if not out_data[0]:
                out_mess = 'No such airport in database'
            else:
                out_mess = out_data[0][1] + '\nairport type: ' + out_data[0][0] + '\nairport coordinates: ' \
                           + str(out_data[0][2]) + ', ' + str(out_data[0][3])
        else:
            out_data.append(search(session, request[0], 'name'))
            if not out_data[0]:
                out_mess = 'First airport ID is wrong: no such airport in database'
                out_data.append(search(session, request[1], 'name'))
                if not out_data[1]:
                    out_mess += '\nSecond airport ID is wrong: no such airport in database'
            else:
                out_data.append(search(session, request[1], 'name'))
                if not out_data[1]:
                    out_mess = 'Second airport ID is wrong: no such airport in database'
                else:
                    out_data.append(distance(session, request[0], request[1]))
                    out_mess = 'Distance between ' + out_data[0] + ' and ' + out_data[1] + ' is ' + str(out_data[2])

    bot.reply_to(message, out_mess)
    out_data.clear()


bot.polling(none_stop=True, interval=0)

